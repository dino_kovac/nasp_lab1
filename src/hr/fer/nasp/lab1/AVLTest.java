
package hr.fer.nasp.lab1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AVLTest {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Need filename for argument! Aborting.");
            System.exit(-1);
        }

        Scanner fileScanner = null;
        String filename = args[0];
        try {
            fileScanner = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("File " + filename + " not found!");
            System.exit(-1);
        }

        AVLTree<Integer> tree = new AVLTree<Integer>();
        while (fileScanner.hasNextInt()) {
            int value = fileScanner.nextInt();
            tree.add(value);
        }

        fileScanner.close();

        boolean quit = false;
        Scanner in = new Scanner(System.in);

        Pattern addPattern = Pattern.compile("^a\\s+(\\d+)$");
        Pattern deletePattern = Pattern.compile("^d\\s+(\\d+)$");
        Pattern quitPattern = Pattern.compile("^q$");

        do {

            tree.print();

            System.out.println("\nWaiting for command. Available commands:\n" + "add: a <number>\n"
                    + "delete: d <number>\n" + "quit: q\n");
            String line = in.nextLine();
            Matcher addMatcher = addPattern.matcher(line);
            Matcher deleteMatcher = deletePattern.matcher(line);
            Matcher quitMatcher = quitPattern.matcher(line);

            if (addMatcher.find()) {
                int value = Integer.parseInt(addMatcher.group(1));
                tree.add(value);
            } else if (deleteMatcher.find()) {
                int value = Integer.parseInt(deleteMatcher.group(1));
                tree.delete(value);
            } else if (quitMatcher.find()) {
                quit = true;
                System.out.println("Quitting..");
            } else {
                System.out.println("Unrecognized command!");
            }
        } while (!quit);

        in.close();

        return;
    }
}


package hr.fer.nasp.lab1;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

public class AVLTree<T extends Comparable<T>> {

    Node<T> root;
    ArrayList<T> nodeList = new ArrayList<T>();

    public void add(T value) {
        if (find(value) == null) {
            addNode(root, value);
        }
    }

    public void print() {
        OutputStreamWriter outWriter = new OutputStreamWriter(System.out);
        try {
            if (root == null) {
                System.out.print("\n### The tree is empty! ###\n");
            } else {
                root.printTree(outWriter);
                System.out.print("\n");
                outWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Node<T> find(T key) {
        return findNode(root, key);
    }

    public Node<T> findNode(Node<T> node, T key) {

        if (node == null) {
            return null;
        }

        if (key.compareTo(node.value) < 0) {
            return findNode(node.left, key);
        } else if (key.compareTo(node.value) > 0) {
            return findNode(node.right, key);
        } else {
            return node;
        }
    }

    private void rebalanceTree(Node<T> node) {
        if (node == null) {
            return;
        }

        if (node.left != null) {
            rebalanceTree(node.left);
        }

        if (node.right != null) {
            rebalanceTree(node.right);
        }

        if (node.parent != null && Math.abs(node.parent.balanceFactor) == 2) {
            // leaf
            rebalance(node);
            refreshBalanceFactors(root);
        }
    }

    private void addNode(Node<T> node, T value) {

        if (node == null) {
            // happens when adding root
            node = new Node<T>();
            node.value = value;
            node.balanceFactor = 0;
            root = node;
            return;
        }

        if (value.compareTo(node.value) < 0) {
            if (node.left != null) {
                addNode(node.left, value);
            } else {
                node.left = new Node<T>();
                node.left.value = value;
                node.left.parent = node;
                refreshBalanceFactors(root);
                rebalance(node);
                refreshBalanceFactors(root);
            }
        } else {
            if (node.right != null) {
                addNode(node.right, value);
            } else {
                node.right = new Node<T>();
                node.right.value = value;
                node.right.parent = node;
                refreshBalanceFactors(root);
                rebalance(node);
                refreshBalanceFactors(root);
            }
        }

        return;
    }

    public void delete(T key) {
        Node<T> node = find(key);

        if (node != null) {
            deleteNode(node);
        }
    }

    /**
     * Points the parent of the old child to the new child.
     * 
     * @param oldChild the child which needs to be severed from the parent
     * @param newChild the new child, may be null
     */
    private void replaceChild(Node<T> oldChild, Node<T> newChild) {
        if (oldChild.parent != null) {
            if (oldChild.parent.left == oldChild)
                oldChild.parent.left = newChild;
            else
                oldChild.parent.right = newChild;

            if (newChild != null)
                newChild.parent = oldChild.parent;
        }
    }

    public void deleteNode(Node<T> soonGone) {

        if (soonGone.left == null && soonGone.right == null) {
            // no children, just remove it
            replaceChild(soonGone, null);

            if (root == soonGone)
                root = null;

            refreshBalanceFactors(root);
        } else if (soonGone.right != null && soonGone.left == null) {
            // child right
            replaceChild(soonGone, soonGone.right);

            if (root == soonGone)
                root = soonGone.right;

            refreshBalanceFactors(root);
        } else if (soonGone.right == null && soonGone.left != null) {
            // child left
            replaceChild(soonGone, soonGone.left);

            if (root == soonGone)
                root = soonGone.left;

            refreshBalanceFactors(root);
        } else {
            // two children - party time
            Node<T> replacement = findMax(soonGone.left);

            // copy replacement to the node which faces deletion
            soonGone.value = replacement.value;
            deleteNode(replacement);

            return;
        }

        rebalanceTree(root);
    }

    /**
     * Finds the minimum node in the (sub)tree.
     * 
     * @param node root node of the (sub)tree
     * @return the minimum node
     */
    private Node<T> findMin(Node<T> node) {
        if (node == null)
            return null;

        while (node.left != null) {
            node = node.left;
        }

        return node;
    }

    /**
     * Finds the maximum node in the (sub)tree.
     * 
     * @param node root node of the (sub)tree
     * @return the minimum node
     */
    private Node<T> findMax(Node<T> node) {
        if (node == null)
            return null;

        while (node.right != null) {
            node = node.right;
        }

        return node;
    }

    /**
     * Rebalances the tree, call after adding/deleting element.
     */
    private void rebalance(Node<T> node) {

        if (node.parent == null) {
            // this is the root
            return;
        }

        if (node.parent.balanceFactor == 0) {
            // already balanced
            return;
        }

        Node<T> parentNode = node.parent;

        if (node.parent.balanceFactor == 1 || node.parent.balanceFactor == -1) {
            // go up the tree
            rebalance(node.parent);
            return;
        } else if (node.parent.balanceFactor == 2) {
            if (node.balanceFactor == -1) {
                /**
                 * <pre>
                 * (2)
                 *    \
                 *    (-1)
                 *    /
                 * (0)
                 * </pre>
                 */
                rotateRight(node, node.left);
            }
            /**
             * <pre>
             * (2)
             *    \
             *    (1)
             *       \
             *       (0)
             * </pre>
             */
            rotateLeft(parentNode, parentNode.right);
            return;
        } else if (node.parent.balanceFactor == -2) {
            if (node.balanceFactor == 1) {
                /**
                 * <pre>
                 *    (-2)
                 *    /
                 * (1)
                 *    \
                 *    (0)
                 * </pre>
                 */
                rotateLeft(node, node.right);
            }
            /**
             * <pre>
             *       (-2) 
             *       / 
             *    (-1) 
             *    / 
             * (0)
             * </pre>
             */
            rotateRight(parentNode, parentNode.left);
            return;
        }

        throw new RuntimeException("The balance factor of the parent is not in {-2, -1, 0, 1, 2}!");
    }

    /**
     * Refreshes all balance factors in a (sub)tree
     * 
     * @param node which node to start from
     * @return the depth of the (sub)tree
     */
    private int refreshBalanceFactors(Node<T> node) {

        if (node == null) {
            return 0;
        }

        int rightDepth, leftDepth;

        rightDepth = refreshBalanceFactors(node.right);
        leftDepth = refreshBalanceFactors(node.left);
        node.balanceFactor = rightDepth - leftDepth;

        return (rightDepth > leftDepth) ? rightDepth + 1 : leftDepth + 1;
    }

    private void rotateRight(Node<T> parent, Node<T> child) {
        Node<T> grandparent = parent.parent;

        parent.left = child.right;
        if (parent.left != null)
            parent.left.parent = parent;

        child.right = parent;
        if (child.right != null)
            child.right.parent = child;

        if (grandparent != null && grandparent.right == parent)
            grandparent.right = child;
        else if (grandparent != null && grandparent.left == parent)
            grandparent.left = child;

        child.parent = grandparent;

        if (root == parent)
            root = child;
    }

    private void rotateLeft(Node<T> parent, Node<T> child) {
        Node<T> grandparent = parent.parent;

        parent.right = child.left;
        if (parent.right != null)
            parent.right.parent = parent;

        child.left = parent;
        if (child.left != null)
            child.left.parent = child;

        if (grandparent != null && grandparent.right == parent)
            grandparent.right = child;
        else if (grandparent != null && grandparent.left == parent)
            grandparent.left = child;

        child.parent = grandparent;

        if (root == parent)
            root = child;
    }

    /**
     * @return true if the tree structure is consistent, false otherwise
     */
    public boolean isConsistent() {
        return checkConsistency(root, nodeList);
    }

    private boolean checkConsistency(Node<T> node, ArrayList<T> nodeList) {

        if (node == null) {
            nodeList.clear();
            return true;
        }

        ArrayList<T> leftNodes = new ArrayList<T>();
        ArrayList<T> rightNodes = new ArrayList<T>();

        boolean leftConsistent = true, rightConsistent = true, consistent = true;

        if (node.left != null)
            leftConsistent = checkConsistency(node.left, leftNodes);
        if (node.right != null)
            rightConsistent = checkConsistency(node.right, rightNodes);

        if (leftConsistent != true || rightConsistent != true) {
            return false;
        }

        // check if all the values in the left subtree are smaller than this
        // nodes value
        if (leftNodes.size() > 0 && node.value.compareTo(leftNodes.get(leftNodes.size() - 1)) <= 0) {
            consistent = false;
        }

        // check if all the values in the right subtree are bigger than this
        // nodes value
        if (rightNodes.size() > 0 && node.value.compareTo(rightNodes.get(0)) >= 0) {
            consistent = false;
        }

        nodeList.addAll(leftNodes);
        nodeList.addAll(rightNodes);
        nodeList.add(node.value);
        Collections.sort(nodeList);

        return consistent;
    }
}

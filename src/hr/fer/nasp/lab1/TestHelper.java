
package hr.fer.nasp.lab1;

public class TestHelper {

    public static void testImplementation(int number, AVLTree<Integer> tree) {
        assert (number > 0);

        for (int i = 0; i < number; i++) {
            tree.print();
            randomAction(tree);
        }
    }

    public static void randomAction(AVLTree<Integer> tree) {
        // add or delete?
        boolean add = false;

        if ((Math.random() * 2) < 1.0) {
            add = true;
        }

        if (tree.nodeList.size() == 0) {
            add = true;
        }

        if (add) {
            int value = (int)(Math.random() * 100000);
            System.out.println("\n[TEST] a " + value);
            tree.add(value);
            if (tree.find(value) == null || !tree.isConsistent()) {
                System.out.println("Consistency check failed!");
                tree.print();
                System.exit(-5);
            }
        } else {
            int value = tree.nodeList.get((int)(Math.random() * tree.nodeList.size()));
            System.out.println("\n[TEST] d " + value);
            tree.delete(value);
            if (tree.find(value) != null || !tree.isConsistent()) {
                System.out.println("Consistency check failed!");
                tree.print();
                System.exit(-5);
            }
        }

    }
}
